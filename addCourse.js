/*
	ACTIVITY: Finish the add course functionality for our app. Use the following as your instructions:

	1. Get the #createCourse element for the HTML file via querySelector.
	2. Make sure that the forms's default behavior is prevented
	3. Get the input values for #courseName, #courseDescription, and #coursePrice
	4. Use a POST request to send the input's values to the /courses endpoint
	5. Make sure that you include the proper authorization header and its value
	6. If successful, redirect the user to /courses.html. If not, show an error alert.
	7. Check to make sure courses are successfully added. 

*/

let addCourseForm = document.querySelector('#createCourse');

addCourseForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let name = document.querySelector('#courseName').value
	let description = document.querySelector('#courseDescription').value
	let price = document.querySelector('#coursePrice').value


		fetch("http://localhost:4000/courses", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")} `
			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				
				window.location.replace("./courses.html")
				
			}else{
				alert("Error: Course not added. Please try again.")
			}
			
	})
})